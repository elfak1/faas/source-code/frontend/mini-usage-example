const btn = document.getElementById("send");
btn.onclick = (ev)=> sendreq(ev.target);
const codebtn = document.getElementById("code");
codebtn.onclick = (ev)=> sendCodeReq(ev.target);

const label = document.getElementById("responseL");
function sendreq(btn){
	let input = document.getElementById("link").value;
	const body =  {
        "arguments":["hello"]
    };
    const fetchData = {
        method: "post",
		headers: {
			 'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        body: JSON.stringify(body)
    };
	if(!input.startsWith("http")){
        input = "http://"+input;
    }
    fetch(input+"/execute",fetchData)
    .then(response =>{
        if(!response.ok) {
            throw new Error(response.statusText);
        }
        else
            return response.text();
        })
        .then(res => label.innerHTML=res)
        .catch(error => label.innerHTML=error);  
}

function sendCodeReq(btn) {
	let input = document.getElementById("link").value;
	 const fetchData = {
        method: "get",
		headers: {
			 'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
     
    };
	if(!input.startsWith("http")){
        input = "http://"+input;
    }
    fetch(input+"/code",fetchData)
    .then(response =>{
        if(!response.ok) {
            throw new Error(response.statusText);
        }
        else
            return response.text();
        })
        .then(res => label.innerHTML=res)
        .catch(error => label.innerHTML=error);  
}

