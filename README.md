# mini-usage-example

Mini HTML page as an usage example of FaaS endpoints

## Usage

Deploy on a server with nginx/lamp/wamp

Putting the FaaS endpoint in the input field will allow you to either call the code enpdoint to see the code or call the calculate endpoint to see the results.

The project is hardcoded to work with our "joda time" example